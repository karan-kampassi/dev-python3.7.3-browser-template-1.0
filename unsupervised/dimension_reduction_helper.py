import pandas as pd
from sklearn.decomposition import TruncatedSVD
from sklearn.decomposition import PCA
from sklearn.decomposition import IncrementalPCA
from sklearn.decomposition import SparsePCA
from sklearn.decomposition import KernelPCA
from visualization_helper import visualizeHelper



class dimensionReduction:

    def __init__(self, data, labels=None):
        self.data = data
        if labels is not None:
            self.labels = labels
            self.labels_flag = 'y'
        else:
            self.labels_flag = 'n'

        self.train_index = range(0,len(data))
        self.visualize = visualizeHelper()

    def pca(self,X_train,n_components=2,random_state = 42,whiten = False, Variance_bar = 'No',sep_observation = 'no'):
        pca = PCA(n_components=n_components, whiten=whiten,random_state=random_state)
        X_train_PCA = pca.fit_transform(X_train)
        X_train_PCA = pd.DataFrame(data=X_train_PCA, index=self.train_index)
        importanceOfPrincipalComponents = pd.DataFrame(data=pca.explained_variance_ratio_)
        importanceOfPrincipalComponents = importanceOfPrincipalComponents.T
        if Variance_bar == 'yes':
            self.visualize.pca_barplot(importanceOfPrincipalComponents)
        if sep_observation == 'yes' and n_components == 2 and self.labels_flag == 'y':
            self.visualize.scatterPlot(X_train_PCA,self.labels)
        else:
            print("Need only 2 components or Label Details to visualize the separation observation")
        return X_train_PCA

    def icreamental_pca(self,X_train,n_components=2,batch_size = None,Variance_bar = 'No',sep_observation='no'):
        incrementalPCA = IncrementalPCA(n_components=n_components,batch_size=batch_size)
        X_train_incrementalPCA = incrementalPCA.fit_transform(X_train)
        X_train_incrementalPCA = pd.DataFrame(data=X_train_incrementalPCA, index=self.train_index)
        importanceOfPrincipalComponents = pd.DataFrame(data=incrementalPCA.explained_variance_ratio_)
        importanceOfPrincipalComponents = importanceOfPrincipalComponents.T
        if Variance_bar == 'yes':
            self.visualize.pca_barplot(importanceOfPrincipalComponents)
        if sep_observation == 'yes' and n_components == 2 and self.labels_flag == 'y':
            self.visualize.scatterPlot(X_train_incrementalPCA,self.labels)
        else:
            print("Need only 2 components or Label Details to visualize the separation observation")
        return X_train_incrementalPCA

    def TruncatedSVD(self,X_train,n_components = 2,algorithm = 'randomized',n_iter = 5,random_state = 42,Variance_bar = 'No',sep_observation='no'):
        svd = TruncatedSVD(n_components=n_components, algorithm=algorithm,n_iter=n_iter, random_state=random_state)
        X_train_svd = svd.fit_transform(X_train)
        X_train_svd = pd.DataFrame(data=X_train_svd, index=self.train_index)
        importanceOfPrincipalComponents = pd.DataFrame(data=svd.explained_variance_ratio_)
        importanceOfPrincipalComponents = importanceOfPrincipalComponents.T
        if Variance_bar == 'yes':
            self.visualize.pca_barplot(importanceOfPrincipalComponents)
        if sep_observation == 'yes' and n_components == 2 and self.labels_flag == 'y':
            self.visualize.scatterPlot(X_train_svd,self.labels)
        else:
            print("Need only 2 components or Label Details to visualize the separation observation")
        return X_train_svd

    def sparse_pca(self,X_train,n_components=2,alpha = 0.0001,random_state = 42,n_jobs = -1,sep_observation='no'):
        sparsePCA = SparsePCA(n_components=n_components, alpha=alpha, random_state=random_state, n_jobs=n_jobs)
        sparsePCA.fit(X_train)
        X_train_sparsePCA = sparsePCA.transform(X_train)
        X_train_sparsePCA = pd.DataFrame(data=X_train_sparsePCA, index= self.train_index)
        if sep_observation == 'yes' and n_components == 2 and self.labels_flag == 'y':
            self.visualize.scatterPlot(X_train_sparsePCA,self.labels)
        else:
            print("Need only 2 components or Label Details to visualize the separation observation")
        return X_train_sparsePCA

    def kernel_pca(self,X_train,random_state = 42,n_components = 2,kernel = 'rbf',gamma = None,n_jobs = 1,sep_observation='no'):
        kernelPCA = KernelPCA(n_components=n_components, kernel=kernel,gamma=gamma, n_jobs=n_jobs, random_state=random_state)
        kernelPCA.fit(X_train)
        X_train_kernelPCA = kernelPCA.transform(X_train)
        X_train_kernelPCA = pd.DataFrame(data=X_train_kernelPCA,index=self.train_index)
        if sep_observation == 'yes' and n_components == 2 and self.labels_flag == 'y':
            self.visualize.scatterPlot(X_train_kernelPCA,self.labels)
        else:
            print("Need only 2 components or Label Details to visualize the separation observation")
        return X_train_kernelPCA


