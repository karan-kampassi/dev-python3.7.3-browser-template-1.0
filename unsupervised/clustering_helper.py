import pandas as pd
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans


class ClusteringHelper:
    def __init__(self):
        self.kmeans = None

    def set_kmeans(self, n_jobs, k):
        self.kmeans = KMeans(n_jobs=n_jobs, n_clusters=k, init='k-means++')

    def apply_kmeans_clustering(self, features, n_jobs, k):
        self.set_kmeans(n_jobs, k)
        self.kmeans.fit(features)
        return self.kmeans

    def get_elbow_plot(self, features, l_range=1, r_range=20):
        inertia_list = []
        for cluster in range(l_range, r_range):
            self.kmeans = KMeans(n_jobs=-1, n_clusters=cluster, init='k-means++')
            self.kmeans.fit(features)
            inertia_list.append(self.kmeans.inertia_)

        # converting the results into a dataframe and plotting them
        frame = pd.DataFrame({'Cluster': range(l_range, r_range), 'inertia_list': inertia_list})
        plt.figure(figsize=(12, 6))
        plt.plot(frame['Cluster'], frame['inertia_list'], marker='o')
        plt.xlabel('Number of clusters')
        plt.ylabel('Inertia')
