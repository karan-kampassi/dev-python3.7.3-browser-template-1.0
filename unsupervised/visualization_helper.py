import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


class visualizeHelper:

    def __init__(self):
        self.fig_size = (10,10)

    def pca_barplot(self,df):

        sns.set(rc={'figure.figsize':self.fig_size})
        sns.barplot(data=df,color='k')

    def scatterPlot(self,xDF, yDF):
        tempDF = pd.DataFrame(data=xDF, index=xDF.index)
        tempDF = pd.concat((tempDF,yDF), axis=1, join="inner")
        tempDF.columns = ["First Vector", "Second Vector", "Label"]
        sns.lmplot(x="First Vector", y="Second Vector", hue="Label", data=tempDF, fit_reg=False)
        ax = plt.gca()