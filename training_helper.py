"""
Created on Mon Mar 1 12:21:01 2021

@author: Prasad Manedeshmukh
"""
import json
import os
import pickle

import pandas as pd
import seaborn as sns
from dotmap import DotMap
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV


class TrainingHelper:

    def __init__(self, model_name, param_json_file="config/model_param_config.json",
                 grid_json_file="config/grid_param_config.json", target_label_names=None):
        self.model_name = model_name
        self.param_json_file = param_json_file
        self.grid_json_file = grid_json_file
        self.estimator_cls = None
        self.target_label_names = target_label_names

    def get_grid_search_params(self):
        """
        This function reads grid_search JSON config file and coverts it into dictionary.
        :return: set of model parameters in the form for dictionary
        """
        model_params = None
        try:
            f = open(self.grid_json_file, "r")
            data_dict = json.loads(f.read())
            model_params = data_dict[self.model_name]
        except FileNotFoundError:
            print("Grid-Search JSON config file not found!")
        return model_params

    def apply_grid_search(self, x_train, y_train, folds=2):
        """
        This function applies grid_search on split training data and labels for the given estimator.
        :param: x_train, y_train - training features, training labels
        :return: cv_classifier- fitted classifier having best model parameters
        """
        self.choose_model()
        cv_classifier = GridSearchCV(estimator=self.estimator_cls, param_grid=self.get_grid_search_params(),
                                     cv=folds)
        print(f"Applying grid-search using {self.estimator_cls.__class__.__name__} estimator!\n")
        try:
            cv_classifier.fit(x_train, y_train)
        except Exception as e:
            print(f"Exception: {e}")
        print(f"Best possible model parameters: {cv_classifier.best_params_}\n")
        return cv_classifier

    def get_model_params(self):
        """
        This function reads model JSON config file and coverts it into dictionary.
        :return: model parameters in the form for dictionary
        """
        model_params = None
        try:
            f = open(self.param_json_file, "r")
            data_dict = json.loads(f.read())
            model_params = DotMap(data_dict[self.model_name])
        except FileNotFoundError:
            print("Model parameters JSON config file not found!")
        return model_params

    def choose_model(self):
        """
        This function chooses the classification estimator with the help of model_name.
        """
        model_params = self.get_model_params()
        if self.model_name == 'random-forest':
            from classification_estimators.rf_estimator import get_rf_estimator
            self.estimator_cls = get_rf_estimator(model_params)
        elif self.model_name == 'knn':
            from classification_estimators.knn_estimator import get_knn_estimator
            self.estimator_cls = get_knn_estimator(model_params)
        elif self.model_name == 'svc':
            from classification_estimators.svc_estimator import get_svc_estimator
            self.estimator_cls = get_svc_estimator(model_params)
        elif self.model_name == 'logistic-regression':
            from classification_estimators.logistic_reg_estimator import get_logistic_estimator
            self.estimator_cls = get_logistic_estimator(model_params)

    def train(self, train_data, train_labels):
        """
        This function trains selected estimator on split training data and labels.
        :params: train_data, train_labels
        """
        self.choose_model()
        self.estimator_cls.fit(train_data, train_labels)
        print(self.estimator_cls)
        print("Successfully trained the model!\n")
        return

    def predict_on_test_data(self, test_data, test_labels):
        """
        This function tests trained estimator on split test data and labels, prints accuracy_score.
        :params: test_data, test_labels
        """
        test_predictions = self.estimator_cls.predict(test_data)
        print(
            f"Accuracy of {self.estimator_cls.__class__.__name__} on test data: {accuracy_score(test_labels, test_predictions)}")
        return test_predictions

    def get_classification_report(self, train_labels, test_predictions):
        clf_report = classification_report(train_labels, test_predictions, target_names=self.target_label_names,
                                           output_dict=True)
        sns.heatmap(pd.DataFrame(clf_report).iloc[:-1, :].T, annot=True)

    def save_trained_model(self):
        """
        This function saves trained estimator in a pickle file.
        """
        if not os.path.exists('models'):
            os.makedirs('models')
        model_filepath = "models/" + self.estimator_cls.__class__.__name__ + ".pkl"
        with open(model_filepath, 'wb') as f:
            pickle.dump(self.estimator_cls, f)
        print(f"Model saved successfully at {model_filepath}!\n")
