"""
Created on Fri Feb 26 15:06:28 2021

@author: amohapatra
"""

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_regression
from sklearn.feature_selection import mutual_info_classif
from sklearn.linear_model import Lasso, Ridge
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeRegressor


class FeatureSelection:
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels

    def split_data(self, test_size):
        """
        This function splits the data in training and testing groups.
        :param: test_size - size if test data set (0.0 - 1)
        :return: x_train, x_test, y_train, y_test
        """
        x_train, x_test, y_train, y_test = None, None, None, None
        try:
            x_train, x_test, y_train, y_test = train_test_split(self.data, self.labels, test_size=test_size)
        except Exception as e:
            print(f"Exception: {e}")
        return x_train, x_test, y_train, y_test

    def select_features(self, x_train, y_train, x_test):
        # configure to select all features
        fs = SelectKBest(score_func=f_regression, k="all")
        # learn relationship from training data
        fs.fit(x_train, y_train)
        # transform train input data
        x_train_fs = fs.transform(x_train)
        # transform test input data
        x_test_fs = fs.transform(x_test)
        return x_train_fs, x_test_fs, fs

    def find_continuous_feature_importance(self):
        """
        This function will return the feature importance with certain regresssor values
        :return:Dataframe with feature importance
        """
        X = self.data
        Y = self.labels

        minmax = MinMaxScaler()
        xScaled = minmax.fit_transform(X)

        lasso = Lasso()
        lasso.fit(xScaled, Y.values)
        lassocoeff = pd.Series((lasso.coef_.flatten()), index=X.columns)
        normLasso = lassocoeff / np.linalg.norm(lassocoeff)
        normLasso = pd.Series(abs(normLasso), index=X.columns)

        ridge = Ridge()
        ridge.fit(xScaled, Y.values)

        ridgecoeff = pd.Series((ridge.coef_.flatten()), index=X.columns)
        normRidge = ridgecoeff / np.linalg.norm(ridgecoeff)
        normRidge = pd.Series(abs(normRidge), index=X.columns)

        dt = DecisionTreeRegressor()
        dt.fit(X, Y)
        feat_importances1 = pd.Series(dt.feature_importances_, index=X.columns)

        rf = RandomForestRegressor()
        rf.fit(X, Y)
        feat_importances2 = pd.Series(rf.feature_importances_, index=X.columns)

        et = ExtraTreesRegressor()
        et.fit(X, Y)
        feat_importances3 = pd.Series(et.feature_importances_, index=X.columns)

        gbr = GradientBoostingRegressor()
        gbr.fit(X, Y)
        feat_importances4 = pd.Series(gbr.feature_importances_, index=X.columns)

        combined_df = {
            'DecisionTree': feat_importances1,
            'RandomForest': feat_importances2,
            'ExtraTrees': feat_importances3,
            'GradientBoostedTrees': feat_importances4,
            'Lasso': normLasso,
            'Ridge': normRidge}

        combined_df = pd.DataFrame(combined_df)
        return combined_df

    def find_categorical_feature_importance(self, categorical_columns):
        """
        This function will return the feature importance with certain regresssor values
        :columns: Categorical Columns
        :return:Dataframe with feature importance
        """
        X = self.data[categorical_columns]
        Y = self.labels
        fs_chi = SelectKBest(score_func=chi2, k='all')
        fs_chi.fit(X, Y)
        feat_importance1 = pd.Series(fs_chi.scores_, index=X.columns)

        fs_mi = SelectKBest(score_func=mutual_info_classif, k='all')
        fs_mi.fit(X, Y)
        feat_importance2 = pd.Series(fs_mi.scores_, index=X.columns)

        combined_df = {
            'Chi': feat_importance1,
            'Mutual_info': feat_importance2}

        combined_df = pd.DataFrame(combined_df)
        return combined_df
