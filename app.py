import boto3
from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')


# make conection to S3 account
def connect_to_s3(aws_access_key_id, aws_secret_access_key):
    session = boto3.Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
    s3_client = session.client('s3')
    return s3_client


# push predictive data to S3
def push_data_to_S3(s3_client, bucket_path):
    s3_client.upload_file("data/vehicle_data.csv", bucket_path, "vehicle_data.csv")
    return


@app.route('/predict', methods=['POST'])
def predict():
    # prediction
    output = "Uploaded prediction data to S3"
    input_path = request.form['s3_input_path']
    output_path = request.form['s3_output_path']
    aws_access_key_id = request.form['s3_access_key']
    aws_secret_access_key = request.form['s3_secret_key']

    s3_client = connect_to_s3(aws_access_key_id, aws_secret_access_key)
    push_data_to_S3(s3_client, output_path)

    return render_template('index.html', prediction_text=format(output))


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
