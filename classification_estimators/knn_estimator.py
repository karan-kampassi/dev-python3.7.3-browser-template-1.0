from sklearn.neighbors import KNeighborsClassifier


def get_knn_estimator(knn_params):
    n_neighbors = knn_params.n_neighbors
    weights = knn_params.weights
    algorithm = knn_params.algorithm
    leaf_size = knn_params.leaf_size
    metric = knn_params.metric
    n_jobs = knn_params.n_jobs
    estimator_cls = KNeighborsClassifier(n_neighbors=n_neighbors, weights=weights, algorithm=algorithm,
                                         leaf_size=leaf_size, metric=metric, n_jobs=n_jobs)
    return estimator_cls
