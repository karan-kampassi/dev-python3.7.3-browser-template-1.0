from sklearn.svm import SVC


def get_svc_estimator(svc_params):
    C = svc_params.C
    kernel = svc_params.kernel
    degree = svc_params.degree
    gamma = svc_params.gamma
    probability = svc_params.probability
    cache_size = svc_params.cache_size
    max_iter = svc_params.max_iter
    decision_function_shape = svc_params.decision_function_shape
    random_state = svc_params.random_state
    estimator_cls = SVC(C=C, kernel=kernel, degree=degree, gamma=gamma, probability=probability, cache_size=cache_size,
                        max_iter=max_iter, decision_function_shape=decision_function_shape, random_state=random_state)
    return estimator_cls
