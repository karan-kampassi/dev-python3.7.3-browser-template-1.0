from sklearn.ensemble import RandomForestClassifier


def get_rf_estimator(random_forest_params):
    random_state = random_forest_params.random_state
    max_features = random_forest_params.max_features
    n_estimators = random_forest_params.n_estimators
    max_depth = random_forest_params.max_depth
    criterion = random_forest_params.criterion
    estimator_cls = RandomForestClassifier(random_state=random_state, max_features=max_features,
                                           n_estimators=n_estimators,
                                           max_depth=max_depth, criterion=criterion)
    return estimator_cls
