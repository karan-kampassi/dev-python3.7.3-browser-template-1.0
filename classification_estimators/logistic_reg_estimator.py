from sklearn.linear_model import LogisticRegression


def get_logistic_estimator(logistic_params):
    penalty = logistic_params.penalty
    dual = logistic_params.dual
    C = logistic_params.C
    fit_intercept = logistic_params.fit_intercept
    intercept_scaling = logistic_params.intercept_scaling
    class_weight = logistic_params.class_weight
    random_state = logistic_params.random_state
    solver = logistic_params.solver
    max_iter = logistic_params.max_iter
    multi_class = logistic_params.multi_class
    n_jobs = logistic_params.n_jobs
    l1_ratio = logistic_params.l1_ratio
    estimator_cls = LogisticRegression(penalty=penalty, dual=dual, C=C, fit_intercept=fit_intercept,
                                       intercept_scaling=intercept_scaling, class_weight=class_weight,
                                       random_state=random_state, solver=solver, max_iter=max_iter,
                                       multi_class=multi_class, n_jobs=n_jobs, l1_ratio=l1_ratio)
    return estimator_cls
