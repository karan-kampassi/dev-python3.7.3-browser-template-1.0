import boto3
import pandas as pd


class S3Utils:
    def __init__(self, aws_access_key_id, aws_secret_access_key):
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key
        self.s3_client = None

    # make conection to S3 account
    def connect_to_s3(self):
        session = boto3.Session(aws_access_key_id=self.aws_access_key_id,
                                aws_secret_access_key=self.aws_secret_access_key)
        self.s3_client = session.client('s3')
        return self.s3_client

    # push file data to S3
    def upload_file_to_s3(self, bucket, bucket_file_path, input_file_path):
        self.s3_client.upload_file(input_file_path, bucket, bucket_file_path)
        return

    # read data from S3
    def read_data_from_s3(self, bucket_name, file_path):
        # get object and file (key) from bucket
        obj = self.s3_client.get_object(Bucket=bucket_name, Key=file_path)
        df = pd.read_csv(obj['Body'])
        return df

    # upload data to s3
    def upload_data_to_s3(self, bucket, bucket_file_path, content):
        self.s3_client.put_object(Body=content, Bucket=bucket, Key=bucket_file_path)
        return
