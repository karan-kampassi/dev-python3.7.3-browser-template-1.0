"""
Created on Wed Mar  3 15:55:13 2021

@author: amohapatra

"""
import pickle
import os
import numpy as np
import pandas as pd
from sklearn.impute import KNNImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler


class PreprocessingHelper:

    def __init__(self, scaler_type=None, binary_columns=None, categorical_columns=None, columns_remove=None):
        self.binary_columns = binary_columns
        self.categorical_columns = categorical_columns
        self.columns_remove = columns_remove
        self.scaler = scaler_type

    def set_binary_columns(self, columns):
        self.binary_columns = columns

    def set_categorical_columns(self, columns):
        self.categorical_columns = columns

    def set_remove_columns(self, columns):
        self.columns_remove = columns

    def set_standard_scaler(self):
        self.scaler = StandardScaler()

    def set_minmax_scaler(self):
        self.scaler = MinMaxScaler()

    def remove_columns(self, df, columns=None):
        """
        This function takes the columns to be removed from the dataset
        :param df: input dataframe
        :param columns: columns to be removed
        :return: dataframe after deleting the columns
        """
        if columns is not None:
            self.set_remove_columns(columns)
        try:
            df = df.drop(labels=self.columns_remove, axis=1)  # drop the labels specified in the columns
        except Exception as e:
            print(f"Exception: {e}")
        return df

    def change_datatype(self,df,column,datatype):
        """
        This function changes the datatype of a cilumn
        :param column:
        :param datatype:
        :return:
        """
        df[column] = df[column].astype(datatype)
        return df

    def remove_duplicates(self, df):
        """
        This function will delete duplicate values if any
        :param df: input dataframe
        :return: dataframe after deleting the columns
        """
        try:
            duplicate_count = df.duplicated().sum()
            if duplicate_count > 0:
                df.drop_duplicates(inplace=True)
        except Exception as e:
            print(f"Exception:{e}")
        return df

    def separate_label_and_features(self, df, label_column_name):
        """
        This function separates the Label column from the feature columns
        :param df: input dataframe
        :param label_column_name: label column for prediction
        :return: feature columns and label column
        """
        data, label = None, None
        try:
            data = self.remove_columns(df, columns=[label_column_name])
            label = df[label_column_name]
        except Exception as e:
            print(f"Exception {e}")
        return data, label

    def get_null_data_points(self, df):
        """
        This function checks if any NULL value present in the dataframe
        :param df: Dataframe
        :return:if Null presents, all the columns with Null values, count of Null values for each column
        """
        null_present = False
        cols_with_missing_values = []
        dataframe_with_null = pd.DataFrame()
        cols = df.columns
        try:
            # check for the count of null values per column
            null_counts = df.isna().sum()
            for i in range(len(null_counts)):
                if null_counts[i] > 0:
                    null_present = True
                    cols_with_missing_values.append(cols[i])
            # write the logs to see which columns have null values
            if null_present:
                dataframe_with_null['columns'] = cols
                dataframe_with_null['missing_values_count'] = np.asarray(null_counts)
        except Exception as e:
            print(f"Exception: {e}")
        return dataframe_with_null, cols_with_missing_values

    def replace_null_values(self, df):
        """
        This function will take the dataframe and check for invalid values in the dataframe and replaces
        it with null
        :param df: input dataframe
        :return: Dataframe with replaced value
        """
        try:
            for column in df.columns:
                count = df[column][df[column] == 'na'].count()
                if count != 0:
                    df[column] = df[column].replace('na', np.nan)
        except Exception as e:
            print(f"Exception:{e}")
        return df

    def scale_columns(self, df, columns, scaler_type='std', flag='train'):
        """
        This function scales the numerical categories using specified scaler
        :param df: input dataframe
        :param scaler_type: type of scaler e.g. std or minmax
        :param flag: train/predict
        :return: scaled dataframe
        """
        if not os.path.exists('models'):
            os.makedirs('models')
        scaler_filepath = "models/" + scaler_type + "_scaler" + ".pkl"
        scaled_num_df = pd.DataFrame()
        try:
            if flag == 'train':
                if scaler_type == 'std':
                    self.set_standard_scaler()
                else:
                    self.set_minmax_scaler()
                fit_data = self.scaler.fit(df[columns])
                # save scaler in a pickle file
                with open(scaler_filepath, 'wb') as f:
                    pickle.dump(fit_data, f)
                scaled_data = self.scaler.transform(df[columns])
                scaled_num_df = pd.DataFrame(data=scaled_data, columns=columns)
                df = self.remove_columns(df, columns)
            elif flag == 'predict':
                with open(scaler_filepath, 'rb') as f:
                    self.scaler = pickle.load(f)
                scaled_data = self.scaler.transform(df[columns])
                scaled_num_df = pd.DataFrame(data=scaled_data, columns=columns)
                df = self.remove_columns(df, columns)
        except Exception as e:
            print(f"Exception:{e}")
        return pd.concat([df, scaled_num_df], axis=1)

    def labelencode_categorical_columns(self, df, columns, flag='train'):
        """
        This function will encode the categorical values present in the dataframe with label encoder
        :param df: input dataframe
        :param columns: binary columns
        :param flag: train/predict
        :return: label encoded dataframe
        """
        if not os.path.exists('models'):
            os.makedirs('models')
        encoder_filepath = "models/" + "label_encoder.npy"
        self.set_binary_columns(columns)
        num_df = self.remove_columns(df, columns)
        cat_df = df[columns]
        le = LabelEncoder()
        try:
            if flag == 'train':
                cat_df = cat_df.apply(le.fit_transform)
                np.save(encoder_filepath, le.classes_)
                df = pd.concat([num_df, cat_df], axis=1)
            elif flag == 'predict':
                le.classes_ = np.load(encoder_filepath, allow_pickle=True)
                cat_df = cat_df.apply(le.fit_transform)
                df = pd.concat([num_df, cat_df], axis=1)
        except Exception as e:
            print(f"Exception:{e}")
        return df

    def onehotencode_categorical_columns(self, df, columns, flag='train'):
        """
        This function will encode the categorical values present in the dataframe.
        :param df: input dataframe
        :param columns: categorical columns
        :param flag: train/predict
        :return: one-hot encoded dataframe
        """
        if not os.path.exists('models'):
            os.makedirs('models')
        encoder_filepath = "models/" + "onehot_encoder.pkl"
        self.set_categorical_columns(columns)
        num_df = self.remove_columns(df, columns)
        cat_df = df[columns]
        ohe = OneHotEncoder(drop='first')
        trans_enc_array = None
        feature_names = []
        if flag == 'train':
            enc_array = ohe.fit(cat_df)
            feature_names = enc_array.get_feature_names()
            with open(encoder_filepath, 'wb') as f:
                pickle.dump(enc_array, f)
            trans_enc_array = ohe.transform(cat_df).toarray()
        elif flag == 'predict':
            with open(encoder_filepath, 'rb') as f:
                oh_encoder = pickle.load(f)
            feature_names = oh_encoder.get_feature_names()
            trans_enc_array = oh_encoder.transform(cat_df).toarray()
        trans_enc_df = pd.DataFrame(data=trans_enc_array, index=None, columns=feature_names)
        df = pd.concat([num_df, trans_enc_df], axis=1)
        return df

    def knn_missing_value_imputer(self, df, n=3):
        """
        :param df: Dataframe
        :param n: Neighbour count
        :return: imputed Dataframe
        """
        new_data = pd.DataFrame()
        try:
            imputer = KNNImputer(n_neighbors=n, weights='uniform', missing_values=np.nan)
            new_array = imputer.fit_transform(df)  # impute the missing values
            # convert the nd-array returned in the step above to a Dataframe
            new_data = pd.DataFrame(data=new_array, columns=df.columns)
        except Exception as e:
            print(f"Exception: {e}")
        return new_data