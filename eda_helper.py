"""
Created on Thu Feb 25 20:26:01 2021

@author: amohapatra
"""
import numpy as np
import pandas as pd

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)


class EDAHelper:

    def __init__(self, df, input_file=None, output_file=None):
        self.input_file = input_file
        self.output_file = output_file
        self.df = df
        self.column_description = None

    def get_all_columns(self):
        """
        This file returns all the columns nammes in a list
        :return: Column list
        """
        return self.df.columns.tolist()

    def get_null_count(self, column_name=None):
        """
        This function returns nul count for the columns passed
        :param column_name: Column names
        :return: Null sum for columns passed else for the entire dataset
        """
        try:
            if column_name is not None:
                return self.df[column_name].isnull().sum()
            else:
                return self.df.isnull().sum()
        except Exception as e:
            print(f"Exception: {e}")

    def get_unique_count(self, column_name=None):
        """
        This function will take column name as input and gives unique count for that column
        :param column_name: Column Name
        :return: Unique  Count
        """
        try:
            if column_name is not None:
                return self.df[column_name].nunique()
            else:
                return self.df.nunique()
        except Exception as e:
            print(f"Exception: {e}")

    def describe_column(self, columns=None):
        """
        This Function works to provide description for all the columns passed
        :param columns: Columns
        :return: Description of Columns
        """
        try:
            if columns is not None:
                self.column_description = self.df[columns].describe()
                return self.column_description
            else:
                self.column_description = self.df.describe()
                return self.column_description
        except Exception as e:
            print(f"Exception: {e}")

    def get_numerical_columns(self):
        #reg_columns = self.df.describe().columns
        #df_new = self.df[reg_columns]
        df_new = self.df._get_numeric_data()
        return df_new

    def detect_outliers(self):
        """
        This function calculates if there are any outlier present in the
        :return: Yields 4 values, column name,Outlier upper limit,utlier lower limit, Number of outliers
        """
        for i in self.df._get_numeric_data().columns:

            Q1 = self.df.describe().at['25%', i]
            Q3 = self.df.describe().at['75%', i]

            IQR = Q3 - Q1
            LTV = Q1 - 1.5 * IQR
            UTV = Q3 + 1.5 * IQR
            x = np.array(self.df[i])
            p = []
            for j in x:
                if j < LTV or j > UTV:
                    p.append(j)
            yield i, Q1, Q3, len(p)

    def show_outlier_details(self):
        """
        This function will show outlier details present in a dataset

        :return: a dataframe with Outliers limit and Number of Outliers
        """
        try:
            Column = []
            oulier_range = []
            Number_Outlier = []
            outlier_gen = self.detect_outliers()
            for i in outlier_gen:
                Column.append(i[0])
                oulier_range.append((i[1], i[2]))
                Number_Outlier.append(i[3])
            combined_df = {
                'Column': Column,
                'Outlier Range': oulier_range,
                'No. Outliers Present': Number_Outlier}
            combined_df = pd.DataFrame(combined_df)
            if self.output_file is not None:
                file1 = open(self.output_file, "a+")
                file1.write("\n#################### OUTLIER DETAILS ####################\n")
                file1.write(str(combined_df))
                file1.close()
                return combined_df
            else:
                return combined_df
        except Exception as e:
            print(f"Exception: {e}")

    def get_redundant_pairs(self):
        """
        This function is a helper function for perform_pearson_correlation()
        :return: Column details
        """
        # Get diagonal and lower triangular pairs of correlation matrix
        pairs_to_drop = set()
        cols = self.df.columns
        try:
            for i in range(0, self.df.shape[1]):
                for j in range(0, i + 1):
                    pairs_to_drop.add((cols[i], cols[j]))
            return pairs_to_drop
        except Exception as e:
            print(f"Exception: {e}")

    def perform_pearson_correlation(self, type='pearson'):
        """
        This function will perform the pearson correlation among all the Numerical features
        :return: correlation matrix among the features
        """
        au_corr = self.df.corr(method=type).abs().unstack()
        labels_to_drop = self.get_redundant_pairs()
        try:
            for abc in labels_to_drop:
                try:
                    au_corr = au_corr.drop(labels=abc)
                except Exception:
                    pass
            return au_corr
        except Exception as e:
            print(f"Exception: {e}")

    def top_pearson_correlation(self):
        """
        This function returns top 10 pearson correlations among all the Numerical features
        :return: correlation matrix among the features
        """
        try:
            au_corr = self.perform_pearson_correlation()
            return au_corr.sort_values(ascending=False)[0:10]
        except Exception as e:
            print(f"Exception: {e}")

    def get_columns_with_zero_std_deviation(self):
        """
        This function will return the columns with Zero Standard Deviation
        :return: Columns which has no Standard deviation
        """
        data_n = self.df._get_numeric_data()
        data_describe = data_n.describe()
        col_to_drop = []
        try:
            for x in data_describe.columns:
                if data_describe[x]['std'] == 0:  # check if standard deviation is zero
                    col_to_drop.append(x)  # prepare the list of columns with standard deviation zero
            if len(col_to_drop) == 0:
                return "No Columns with Zero Standard Deviation"
            else:
                return col_to_drop
        except Exception as e:
            print(f"Exception: {e}")

