import pickle
import pandas as pd


class PredictionHelper:
    def __init__(self, model_path, output_file_path):
        self.model_path = model_path
        self.output_file_path = output_file_path
        self.model = None

    def load_trained_model(self):
        """
        This function loads saved classification model.
        :return:
        """
        with open(self.model_path, 'rb') as f:
            self.model = pickle.load(f)
        print(f"Loaded trained model:{self.model.__class__.__name__} successfully!")

    def predict_on_df(self, df):
        """
        This function predicts labels on pandas dataframe
        :param df:input dataframe containing new data
        :return: output dataframe consisting output labels
        """
        label_array = self.model.predict(df)
        return pd.DataFrame(data=label_array, columns=['output_label'])

    def save_prediction_df(self, df, label_df):
        """
        This function concatenates original dataframe and output labels dataframe and saves it in a CSV file.
        :param df: input dataframe
        :param label_df: consisting output labels
        :return:
        """
        df = pd.concat([df, label_df], axis=1)
        df.to_csv(self.output_file_path, encoding='UTF-8', index=False)
        print(f"Saved prediction CSV file at {self.output_file_path} successfully!")

    def predict_single_input(self, input_feature):
        """
        This function predictions label on single input data point. (For Test Purpose)
        :param input_feature: array of input features
        :return: output label
        """
        output_label = self.model.predict(input_feature)
        return output_label[0]
